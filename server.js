// server.js
// where your node app starts

// init project
const express = require("express");
const app = express();
const fs = require("fs");
const bodyParser = require('body-parser');
const cors = require('cors');
const hcaptcha = require('express-hcaptcha');

// we've started you off with Express,
// but feel free to use whatever libs or frameworks you'd like through `package.json`.

app.use(cors());
app.use(bodyParser.json());
const rateLimit = require("express-rate-limit");
const RedisStore = require('rate-limit-redis');

// Add rate-limits to the API endpoint for 5 minutes after 10 requests.
const apiLimiter = rateLimit({
  windowMs: 5 * 60 * 1000, // 15 minutes
  max: 10
});
app.use("/api/", apiLimiter);

const addDomainLimiter = rateLimit({
  windowMs: 60 * 60 * 1000, // 1 hour window
  max: 5, // start blocking after 5 requests
  message:
    "Too many Glitch apps added from this IP address, try again after a hour.",
});

// http://expressjs.com/en/starter/static-files.html
app.use(express.static("public"));

// http://expressjs.com/en/starter/basic-routing.html
app.get("/", function(request, response) {
  response.sendFile(__dirname + "/views/index.html");
});

app.get("/admin", function(request, response) {
  response.sendFile(__dirname + "/views/admin.html");
});

app.get("/legal", function(request, response) {
  response.sendFile(__dirname + "/views/legal.html");
});

app.get("/api/urls/add", function(request, response) {
  var url = request.query["url"];

  if (url != undefined) {
    addUrl(url)
      .then(() => {
        response.send({
          ok: true,
          description: "Success! Your Glitch app will be started to be pinged after some time.",
          request_data: {
            url: url,
            status: "active"
          }
        });
      })
      .catch(reason => {
        if (reason == "URL_IN_DB") {
          response.send({
            ok: false,
            decription:
              "That URL is now on our database. Please ask us to remove it in our GitLab Issue Tracker if you want.",
            request_data: { url: url, status: "active" }
          });
        } else {
          response.send({
            ok: false,
            description: "Something went berserk. Please try again.",
            request_data: { url: url, status: "went_berserk"}
          });
        }
      });
  } else {
    response.send({
      ok: false,
      description: "Something went berserk. Please try again.",
      request_data: {
        url: url,
        status: "went_berserk"
      }
    });
  }
});

function addUrl(url) {
  return new Promise((res, rej) => {
    fs.readFile(".data/urls.json", "utf8", function(err, contents) {
      var j = JSON.parse(contents);
      if (j.indexOf(url) > -1) {
        rej("URL_IN_DB");
      } else {
        j.push(url);
      }
      fs.writeFile(".data/urls.json", JSON.stringify(j), "utf8", () => {
        res();
      });
    });
  });
}

app.all('/api/urls/active', (req, res) => {
  const UrlsInDB = require("./.data/urls.json")
  res.send({ok: true, description: "API request to pull latest database dump accepted.", dump: UrlsInDB})
})

const SECRET = process.env.HCAPTCHA_SECRET_KEY;
app.post('/api/verifyUserOnSubmit', hcaptcha.middleware.validate(SECRET), (req, res) => {
  res.json({message: 'verified!', hcaptcha: req.hcaptcha});
});

app.use(function (req, res, next) {
  res.status(404).send({ok: false, description: "API endpoint not found."})
})

// listen for requests :)
const listener = app.listen(process.env.PORT, function() {
  console.log("Your app is listening on port " + listener.address().port)

  // Start ping

  require("./ping.js");
});
